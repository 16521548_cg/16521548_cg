#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include "FillColor.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE

	SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
	Vector2D p1(150, 300);
	Vector2D p2(200, 500);
	Vector2D p3(300, 600);
	Vector2D p4;
	int n = 0;
	cout << "Ban muon ve duong cong Bezier bac may? ";
	cin >> n;
	if(n == 2)
		DrawCurve2(ren, p1, p2, p3);
	if (n == 3) {
		p4.set(400, 400);
		DrawCurve3(ren, p1, p2, p3, p4);
	}
	SDL_Color A;
	A.r = 0; A.b = 255; A.g = 0; A.a = 255;
	p1.set(200, 300);
	p2.set(100, 200);
	p3.set(300, 100);
	//TriangleFill(p1, p2, p3, ren, A);
	p1.set(200, 100);
	p2.set(400, 150);
	//FillIntersectionRectangleCircle(p1, p2, 200, 200, 100, ren, A);
	//FillIntersectionTwoCircles(200, 200, 100, 300, 100, 100, ren, A);
	//CircleFill(100, 100, 100, ren, A);
	p1.set(100, 300);
	p2.set(300, 500);
	//RectangleFill(p1, p2, ren, A);
	FillIntersectionEllipseCircle(200, 300, 100, 200, 200, 200, 100, ren, A);
	p1.set(150, 300);
	p2.set(200, 500);
	p3.set(300, 600);
	p4.set(400, 400);

	//SDL_RenderPresent(ren);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event

	bool running = true;

	while (running)
	{
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{

			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
			if (event.type == SDL_MOUSEMOTION) 
			{
				int px[4];
				int py[4];
				px[0] = p1.x; px[1] = p2.x; px[2] = p3.x; px[3] = p4.x;
				py[0] = p1.y; py[1] = p2.y; py[2] = p3.y; py[3] = p4.y;
				int c = 0;
				int X = event.motion.x;
				int Y = event.motion.y;
				for (int i = 0; i < n + 1; i++) {
					float inside = (X - px[i])*(X - px[i]) + (Y - py[i])*(Y - py[i]);
					if (inside <= 30*30)//R*R
					{
						c = i+1;
						break;
					}
				}
				if (c != 0) {
					if (event.motion.state & SDL_BUTTON_LMASK)
					{
						int x = event.motion.x;
						int y = event.motion.y;
						SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
						SDL_RenderClear(ren);
						if (c == 1) {
							p1.x = x;
							p1.y = y;
						}
						if (c == 2) {
							p2.x = x;
							p2.y = y;
						}
						if (c == 3) {
							p3.x = x;
							p3.y = y;
						}
						if (c == 4) {
							p4.x = x;
							p4.y = y;
						}
						SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
						if(n==2)
							DrawCurve2(ren, p1, p2, p3);
						if(n==3)
							DrawCurve3(ren, p1, p2, p3, p4);
						SDL_RenderPresent(ren);
					}
				}
			}
			
		}
	}

	SDL_DestroyRenderer(ren);
	//SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
