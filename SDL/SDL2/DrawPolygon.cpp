#include "DrawPolygon.h"
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3], y[3];

	float phi = M_PI / 2;
	for (int i = 0; i < 3; i++)
	{

		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (2 * M_PI) / 3;
	}
	for (int i = 0; i < 3; i++)
		Midpoint_Line(x[i], y[i], x[(i + 1) % 3], y[(i + 1) % 3], ren);

}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4], y[4];

	float phi = M_PI / 4;
	for (int i = 0; i < 4; i++)
	{

		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (2 * M_PI) / 4;
	}
	for (int i = 0; i < 4; i++)
		Midpoint_Line(x[i], y[i], x[(i + 1) % 4], y[(i + 1) % 4], ren);

}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];

	float phi = M_PI / 2;
	for (int i = 0; i < 5; i++)
	{

		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (2 * M_PI) / 5;
	}
	for (int i = 0; i < 5; i++)
		Midpoint_Line(x[i], y[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6], y[6];

	float phi = M_PI / 6;
	for (int i = 0; i < 6; i++)
	{

		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (2 * M_PI) / 6;
	}
	for (int i = 0; i < 6; i++)
		Midpoint_Line(x[i], y[i], x[(i + 1) % 6], y[(i + 1) % 6], ren);
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];

	float phi = M_PI / 2;
	for (int i = 0; i < 5; i++)
	{

		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (2 * M_PI) / 5;
	}

	int a[5] = { 2,4,1,3,0 };
	int i = 0, j = 0;
	while (i != 6)
	{
		Midpoint_Line(x[i], y[i], x[a[j]], y[a[j]], ren);
		i = a[j];
		if (i == 0) break;
		j++;
	}
}
void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int r = sin(M_PI / 10)*(R / sin((7 * M_PI) / 10));
	int x[10], y[10];
	float phi = M_PI / 2;

	for (int i = 0; i < 10; i += 2)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (2 * M_PI) / 5;
	}
	phi = 7 * M_PI / 10;
	for (int j = 1; j < 10; j += 2)
	{
		x[j] = xc + int(r*cos(phi) + 0.5);
		y[j] = yc - int(r*sin(phi) + 0.5);
		phi += (2 * M_PI) / 5;
	}

	for (int i = 0; i < 10; i++)
		Midpoint_Line(x[i], y[i], x[(i + 1) % 10], y[(i + 1) % 10], ren);
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int r = (R * sin(M_PI / 8)) / sin(3 * M_PI / 4);
	int x[16], y[16];
	float phi = M_PI / 2;

	for (int i = 0; i < 16; i += 2)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += M_PI / 4;
	}
	phi = 5 * M_PI / 8;
	for (int j = 1; j < 16; j += 2)
	{
		x[j] = xc + int(r*cos(phi) + 0.5);
		y[j] = yc - int(r*sin(phi) + 0.5);
		phi += M_PI / 4;
	}

	for (int i = 0; i < 16; i++)
		Midpoint_Line(x[i], y[i], x[(i + 1) % 16], y[(i + 1) % 16], ren);
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int r = sin(M_PI / 10)*(R / sin((7 * M_PI) / 10));
	int x[10], y[10];
	float phi = startAngle;

	for (int i = 0; i < 10; i += 2)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (2 * M_PI) / 5;
	}
	phi = startAngle + M_PI / 5;
	for (int j = 1; j < 10; j += 2)
	{
		x[j] = xc + int(r*cos(phi) + 0.5);
		y[j] = yc - int(r*sin(phi) + 0.5);
		phi += (2 * M_PI) / 5;
	}

	for (int i = 0; i < 10; i++)
		Midpoint_Line(x[i], y[i], x[(i + 1) % 10], y[(i + 1) % 10], ren);
	if (r > 1)
		DrawStarAngle(xc, yc, r, startAngle + M_PI, ren);
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	DrawStarAngle(xc, yc, r, M_PI / 2, ren);
}
