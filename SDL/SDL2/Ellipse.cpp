#include "Ellipse.h"
#include<iostream>
void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, -y + yc);
	SDL_RenderDrawPoint(ren, x + xc, -y + yc);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int x = 0, y = b;
	float p = -2 * a*a*b + a * a + 2 * b*b;
	for (; b*b*x <= a * a*y; x++)
	{
		Draw4Points(xc, yc, x, y, ren);
		if (p >= 0)
		{
			p += 4 * a*a * (1 - y);
			y--;
		}
		p += b * b * ((4 * x) + 6);
	}
	//Area 2
	y = 0;
	x = a;
	p = -2 * b*b*a + b * b + 2 * a*a;
	for (; a*a*y <= b * b * x; y++)
	{
		Draw4Points(xc, yc, x, y, ren);
		if (p >= 0)
		{
			p += 4 * b*b * (1 - x);
			x--;
		}
		p += a * a * ((4 * y) + 6);
	}

}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int x = 0, y = b;
	float p = float(a*a) / 4 - a * a*b + b * b;
	Draw4Points(xc, yc, x, y, ren);
	for (; b*b*x <= a * a*y; x++)
	{

		if (p < 0)
		{
			p += 2 * b*b*x + b * b;
		}
		else
		{
			p += 2 * b * b*x + b * b - 2 * a*a*y;
			y--;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
	p = b * b * ((float)x + 0.5)*((float)x + 0.5) + a * a *(y - 1)*(y - 1) - a * a*b*b;
	Draw4Points(xc, yc, x, y, ren);
	for (; y >= 0; y--)
	{
		if (p < 0)
		{

			p += 2 * b*b*x - 2 * a*a*y - a * a;
			x++;
		}
		else
		{
			p += -2 * a*a*y + a * a;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
}
