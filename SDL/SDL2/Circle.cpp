#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    int new_x;
    int new_y;

    new_x = xc + x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
	SDL_RenderDrawPoint(ren, -x + xc, y + yc);
	SDL_RenderDrawPoint(ren, -y + xc, x + yc);
	SDL_RenderDrawPoint(ren, -y + xc, -x + yc);
	SDL_RenderDrawPoint(ren, -x + xc, -y + yc);
	SDL_RenderDrawPoint(ren, x+ xc, -y+ yc);
	SDL_RenderDrawPoint(ren, y+ xc, -x+ yc);
	SDL_RenderDrawPoint(ren, y + xc, x+ yc);

    //7 points
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0, y = R;
	Draw8Points(xc, yc, x, y, ren);
	int p = 1 - R;
	while (x <= y)
	{
		if (p < 0) {
			p += 2*x + 3;
		}
		else {
			p += 2*(x - y) + 5;
			y = y - 1;
		}
		x = x + 1;
		Draw8Points(xc, yc, x, y, ren);
	}
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0, y = R;
	int p = 1 - R;
	Draw8Points(xc, yc, x, y, ren);
	while (x <= y)
	{

		int const1 = 2 * x + 3;
		int const2 = 2 * (x - y) + 5;
		if (p < 0)
		{
			p += const1;
		}
		else
		{
			p += const2;
			y--;
		}
		x++;
		Draw8Points(xc, yc, x, y, ren);
	}
}
