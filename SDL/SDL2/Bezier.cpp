#include "Bezier.h"
#include <iostream>
#include "Circle.h"
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	Vector2D A;
	for (float t = 0; t < 1; t += 0.00001)
	{
		A.x = (1-t)*(1-t)*p1.x + 2 * (1-t)*t*p2.x + t*t*p3.x;
		A.y = (1-t)*(1-t)*p1.y + 2 * (1-t)*t*p2.y + t*t*p3.y;
		SDL_RenderDrawPoint(ren, A.x, A.y);
	}
	MidpointDrawCircle(p1.x, p1.y, 30, ren);
	MidpointDrawCircle(p2.x, p2.y, 30, ren);
	MidpointDrawCircle(p3.x, p3.y, 30, ren);
	
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	Vector2D A;
	for (float t = 0; t < 1; t += 0.00001)
	{
		A.x = (1 - t)*(1 - t)*(1 - t)*p1.x + 3 * (1 - t)*(1 - t)*t*p2.x + 3 * (1 - t)*t * t*p3.x + t * t*t*p4.x;
		A.y = (1 - t)*(1 - t)*(1 - t)*p1.y + 3 * (1 - t)*(1 - t)*t*p2.y + 3 * (1 - t)*t * t*p3.y + t * t*t*p4.y;
		SDL_RenderDrawPoint(ren, A.x, A.y);
	}
	MidpointDrawCircle(p1.x, p1.y, 30, ren);
	MidpointDrawCircle(p2.x, p2.y, 30, ren);
	MidpointDrawCircle(p3.x, p3.y, 30, ren);
	MidpointDrawCircle(p4.x, p4.y, 30, ren);

}


